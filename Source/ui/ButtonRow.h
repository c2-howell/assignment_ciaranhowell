//
//  ButtonRow.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/22/15.
//
//

#ifndef __JuceBasicAudio__ButtonRow__
#define __JuceBasicAudio__ButtonRow__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"

/** ButtonRow Class. This Class allows for 8 TextButtons, an OnOffStatus for each of those TextButtons and a comboBox (noteSelector) to be grouped into one class. It also includes functions that the status of these TextButtons, can be retrieved and set by MainComponent. It also includes a listener that reacts when a button clicked. This function is then overridden in MainComponent where all the other ButtonRows are checked to see if that same beat has been selected on any other rows and then switches that button off if there is. */
class ButtonRow : public Component,
public ButtonListener,
public ComboBoxListener

{
public:
    /** Constructor */
    ButtonRow ();
    
    /** Destructor */
    ~ButtonRow();
    
    /** TextButton Listener Callback function. Listens for a button click, then changes the corresponding onOffStatus.
     @ param button. A pointer to the TextButton that has been changed .*/
    void buttonClicked (Button* button) override;
    
    /** ComboBox Listener Callback function. Listens for changes to the ComboBox in ButtonRow (noteSelector) and sets the corresponding variable (noteSelected) to the value of that ComboBox.
     @ param comboBoxThatHasChanged. A pointer to the ComboBox that has been changed (noteSelector).*/
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
    /** Returns the value of the noteSelector ComboBox.
    @ return noteSelected. This value is the position in the scale for that particular ButtonRow. */
    int getNoteSelected();
    
    /** Returns the onOffStatus of particular button.
     @ param beat. The button in the row that needs to be checked.
     @return onOffStatus. This function returns the onOffStatus for the selected button*/
    int getOnOffStatus(int beat);
    
    /** Sets the onOffStatus to 0 (off) for a particular button in that row. This is used to ensure that no 2 rows have a button selected on the same beat.
     @ param selectedbeat. The number of the button of the selected row, that you wish to turn off. */
    void setOnOffStatus(int selectedBeat);
    
    /** ButtonRow Listener Class. A listener for other classes to inherit (MainComponent) which is called when a TextButton is selected. This allows MainComponent to check all other ButtonRows and turn off any buttons that are on, on the same beat.  */
    class Listener
    {
    public:
        /** Virtual Destructor. */
        virtual~Listener() {}
        
        /** ButtonRow::Listener Callback function.  Receives the beat of the button that has been clicked, but before it has been turned on.
         @ param selectedBeat. The beat of the button that is about to be turned on. */
        virtual void beatTurnedOn (const unsigned int selectedBeat)=0;
        
    };
    
    /** Registers a listener that will be called when a TextButton in the row is turned on. */
    void setListener(Listener* newListener);
    
    /** Deregisters a previously-registered listener. */
    void removeListener(Listener* listener);
    
private:
    
    TextButton buttons[8];
    ComboBox noteSelector;
    Listener* listener;
    int onOffStatuses[8];
    
    int noteSelected;
    
};

#endif /* defined(__JuceBasicAudio__ButtonRow__) */

