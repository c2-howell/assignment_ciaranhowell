//
//  ButtonRow.cpp
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/22/15.
//
//

#include "ButtonRow.h"

ButtonRow::ButtonRow()
{
    //Initialize the array of TextButtons
    for (int i=0;i<8;i++)
    {
        buttons[i].setBounds(((i+1)*2.5)+(i*50), 10, 50, 50);
        buttons[i].setColour(TextButton::buttonColourId,Colours::royalblue);
        addAndMakeVisible(&buttons[i]);
        onOffStatuses[i]=0;
        buttons[i].addListener(this);
    }
    
    //Initialize the Note Selector
    noteSelector.setBounds(423, 10, 70, 50);
    noteSelector.addItem("1", 1);//you cant set this to 0
    noteSelector.addItem("2", 2);
    noteSelector.addItem("3", 3);
    noteSelector.addItem("4", 4);
    noteSelector.addItem("5", 5);
    noteSelector.addItem("6", 6);
    noteSelector.addItem("7", 7);
    noteSelector.addItem("8", 8);
    noteSelector.setSelectedId(1);
    noteSelector.addListener(this);
    addAndMakeVisible(&noteSelector);
    
    //Initialze the pointer to a nullptr
    listener=nullptr;
    
}

ButtonRow::~ButtonRow()
{
    
}

void ButtonRow::buttonClicked(Button* button)
{
    
    for (int i=0; i<8; i++)
    {
        Button* const thisButton = &buttons[i];
        
        if (button==thisButton)
        {
            if (onOffStatuses[i]==0) {
               
                if (listener!=nullptr) {
                    listener->beatTurnedOn(i);
                }
                
                onOffStatuses[i] = 1;
                thisButton->setColour(TextButton::buttonColourId,Colours::red);
                
            }
            else if (onOffStatuses[i]==1){
                onOffStatuses[i] = 0;
                thisButton->setColour(TextButton::buttonColourId,Colours::hotpink);
                
            }
            
            break;
        }
        
    }
    
}

void ButtonRow::comboBoxChanged(ComboBox* comboBoxThatHasChanged)
{
    noteSelected=noteSelector.getSelectedId();
}

int ButtonRow::getNoteSelected()
{
    return noteSelected-1;
    
}

int ButtonRow::getOnOffStatus(int beat)
{
    return onOffStatuses[beat];
    
}

void ButtonRow::setOnOffStatus(int selectedBeat)
{
    onOffStatuses[selectedBeat]=0;
    buttons[selectedBeat].setColour(TextButton::buttonColourId,Colours::royalblue);
    
}

void ButtonRow::setListener(Listener* newListener)
{
    listener=newListener;
}

void ButtonRow::removeListener(Listener* newListener)
{
    listener=nullptr;
}

