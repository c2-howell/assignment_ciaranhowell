/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "ButtonRow.h"
#include "Counter.h"



//==============================================================================
/*
 This component lives inside our window, and this is where you should put all
 your controls and content.
 */

/** MainComponent Class. This Component is the top layer of the sequencer. It is where the refrences to an Audio object and a Counter object are initialised. It is also where all Sliders, Buttons, ComboBoxes and ButtonRows are created and made visible. As well as having a listener for all components in the class, there are also listeners for the Counter and ButtonRow which allows for values generated within these classes, to be sent to MainComponent and then sent to any other classes that need these values. */
class MainComponent   : public Component,
public MenuBarModel,
public Slider::Listener,
public ComboBox::Listener,
public TextButton::Listener,
public Counter::Listener,
public ButtonRow::Listener

{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& audio_, Counter& counter_);
    
    /** Destructor */
    ~MainComponent();
    
    /** Slider Listener Callback function. Listens for changes to Sliders in MainComponent (gainSlider and tempoSlider) and sets the corresponding variables to the value of that slider.
     @ param slider. A pointer to the slider that has been moved.*/
    void sliderValueChanged (Slider* slider) override;
    
    /** ComboBox Listener Callback function. Listens for changes to ComboBoxes in MainComponent (oscSelector, keySelector and scaleSelector) and sets the corresponding variables to the value of that ComboBox.
     @ param comboBoxThatHasChanged. A pointer to the ComboBox that has been changed.*/
    void comboBoxChanged (ComboBox* comboBoxThatHasChanged) override;
    
    /** TextButton Listener Callback function. Listens for changes to the playStopButton and changes the playStopStatus of the sequencer accordingly
     @ param button. A pointer to the TextButton that has been changed (playStopButton).*/
    void buttonClicked (Button* button) override;
    
    /** Counter Listener Callback function. Listens for changes to the Counter object and reacts accordingly, calling generateNote function if playStopState==1 and doing nothing if playStopState==0
     @ param currentBeat. The value of the beat that the sequencer is currently on. */
    void beatChanged (const unsigned int currentBeat) override;
    
    /** ButtonRow Listener Callback function. Listens for when a button is clicked within a ButtonRow object and then checks all other rows to see if that beat has been selected. It then turns that button and corresponding onOffStatuses off, leaving the button that was clicked to be the only row with that beat selected.     
     @ param currentBeat. The value of the beat that the sequencer is currently on. */
     void beatTurnedOn (const unsigned int selectedBeat) override;
    
    /** Returns current sample rate.
     @return sample rate*/
    int getSR();
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
private:
    Audio& audio;
    Counter& counter;
    Slider gainSlider,  tempoSlider;
    ComboBox oscSelector, keySelector, scaleSelector;
    Label gainLabel, oscLabel, tempoLabel, keyScaleLabel;
    TextButton playStopButton;
    
    ButtonRow buttonRow[4];
    
    int playStopState, oscNum;
    
    
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
