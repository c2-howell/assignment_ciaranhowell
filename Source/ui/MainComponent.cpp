/*
 ==============================================================================
 
 This file was auto-generated!
 
 ==============================================================================
 */

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_, Counter& counter_) : audio (audio_), counter(counter_)
{
    //Intialise gainSlider
    setSize (500, 400);
    gainSlider.setBounds(135, (getHeight()/2)+70, 60, 80);
    gainSlider.setSliderStyle(Slider::Rotary);
    gainSlider.setRange(0, 1);
    gainSlider.addListener(this);
    gainSlider.setValue(0.75);
    addAndMakeVisible(&gainSlider);
    
    //Initialise tempoSlider
    tempoSlider.setSliderStyle(Slider::LinearHorizontal);
    tempoSlider.setRange(85, 240);
    tempoSlider.setValue(120.0);
    tempoSlider.setBounds(5, getHeight()/2+70, 120, 80);
    tempoSlider.addListener(this);
    addAndMakeVisible(&tempoSlider);
    
    //Initialize oscSelector
    oscSelector.addItem("Sine", 1);
    oscSelector.addItem("Square", 2);
    oscSelector.addItem("Sawtooth", 3);
    oscSelector.addItem("Triangle", 4);
    oscSelector.setSelectedId(1,dontSendNotification);
    oscSelector.setBounds(0, (getHeight()/2)+150, 200, 50);
    oscSelector.addListener(this);
    addAndMakeVisible(&oscSelector);
    
    //Initialize keySelector
    keySelector.addItem("C", 48);
    keySelector.addItem("C#", 49);
    keySelector.addItem("D", 50);
    keySelector.addItem("D#", 51);
    keySelector.addItem("E", 52);
    keySelector.addItem("F", 53);
    keySelector.addItem("F#", 54);
    keySelector.addItem("G", 55);
    keySelector.addItem("G#", 56);
    keySelector.addItem("A", 57);
    keySelector.addItem("A#", 58);
    keySelector.addItem("B", 59);
    keySelector.setSelectedId(48);
    keySelector.setBounds((getWidth()/2)+50, (getHeight()/2)+100, 200, 50);
    keySelector.addListener(this);
    addAndMakeVisible(&keySelector);
    
    //Initialize scaleSelector
    scaleSelector.addItem("Major", 1); //can't set this to zero
    scaleSelector.addItem("Natural Minor", 2);
    scaleSelector.addItem("Harmonic Minor", 3);
    scaleSelector.setSelectedId(1);
    scaleSelector.setBounds((getWidth()/2)+50, (getHeight()/2)+150, 200, 50);
    scaleSelector.addListener(this);
    addAndMakeVisible(&scaleSelector);
    
    // Initialize all Labels
    tempoLabel.setBounds(5, (getHeight()/2)+55, 60, 60);
    keyScaleLabel.setBounds(300, (getHeight()/2)+55, 100, 60);
    gainLabel.setBounds(135, getHeight()/2+55, 60, 60);
    oscLabel.setBounds(0, (getHeight()/2)+110, 100, 50);
    keyScaleLabel.setText("Key/Scale Select: ", dontSendNotification);
    tempoLabel.setText("Tempo: ", dontSendNotification);
    gainLabel.setText("Gain: ", dontSendNotification);
    oscLabel.setText("Waveform Select:", dontSendNotification);
    keyScaleLabel.setInterceptsMouseClicks(false, false);
    tempoLabel.setInterceptsMouseClicks(false, false);
    gainLabel.setInterceptsMouseClicks(false, false);
    oscLabel.setInterceptsMouseClicks(false, false);
    addAndMakeVisible(keyScaleLabel);
    addAndMakeVisible(tempoLabel);
    addAndMakeVisible(gainLabel);
    addAndMakeVisible(oscLabel);
    
    // Initialize array of ButtonRows
    for (int i=0; i<4; i++){
        buttonRow[i].setBounds(0, i*70, getWidth(), getHeight());
        buttonRow[i].setInterceptsMouseClicks (false, true);
        buttonRow[i].setListener(this);
        addAndMakeVisible(&buttonRow[i]);
    }
    
    //Initialize the Play/Stop Button
    playStopButton.setBounds(210,getHeight()/2+110, 80,80);
    playStopButton.addListener(this);
    playStopButton.setColour(TextButton::buttonColourId, Colours::green);
    playStopButton.setButtonText("PLAY");
    addAndMakeVisible(&playStopButton);
    playStopState=0;
    
    // Set the Counter::Listener
    counter.setListener(this);
    
}

MainComponent::~MainComponent()
{
    counter.removeListener(this);
    for (int i=0; i<4; i++){
        buttonRow[i].removeListener(this);
    }
}


void MainComponent::sliderValueChanged (Slider* slider)
{
    if (slider==&gainSlider) {
        audio.setGain(gainSlider.getValue());
    }
    
    else if (slider==&tempoSlider)
    {
        counter.setInterval(tempoSlider.getValue());
    }
    
}

void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged==&oscSelector)
    {
        audio.getID(oscSelector.getSelectedId());
    }
    else if (comboBoxThatHasChanged==&keySelector);
    {
        audio.setKey(keySelector.getSelectedId());
    }
    if (comboBoxThatHasChanged==&scaleSelector) {
        audio.setScale(scaleSelector.getSelectedId());
    }
    
}

void MainComponent::buttonClicked (Button* button)
{
    if (playStopState==0)
    {
        counter.setPlayStopState(1);
        playStopButton.setColour(TextButton::buttonColourId, Colours::red);
        playStopButton.setButtonText("STOP");
        playStopState=1;
    }
    else if (playStopState==1)
    {
        counter.setPlayStopState(0);
        playStopButton.setColour(TextButton::buttonColourId, Colours::green);
        playStopButton.setButtonText("PLAY");
        playStopState=0;
    }
    
}

void MainComponent::beatChanged(const unsigned int currentBeat)
{
    for(int i=0;i<4;i++){
        if (buttonRow[i].getOnOffStatus(currentBeat-1)==1)
        {
            audio.generateNote(oscSelector.getSelectedId(), gainSlider.getValue(), buttonRow[i].getNoteSelected());
        }
        
    }
    
}

void MainComponent::beatTurnedOn(const unsigned int selectedBeat)
{
    for (int whichRow=0; whichRow<4; whichRow++) {
        if(buttonRow[whichRow].getOnOffStatus(selectedBeat)==1)
        {
            buttonRow[whichRow].setOnOffStatus(selectedBeat);
        }
    }
    
}



//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            //            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
            //                                                            0, 2, 2, 2, true, true, true, false);
            //            audioSettingsComp.setSize (450, 350);
            //            DialogWindow::showModalDialog ("Audio Settings",
            //                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

