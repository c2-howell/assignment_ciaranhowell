//
//  Counter.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/22/15.
//
//

#ifndef __JuceBasicAudio__Counter__
#define __JuceBasicAudio__Counter__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"
/** Counter thread.
 This class derives from Juce's thread class. This thread starts as soon as it is created, but only calls the beatChanged() function when the playStopStatus=1.
 */

class Counter : public Thread
{
public:
    /** Constructor. Starts the thread and intialises listener, beat and playStopState.*/ 
    Counter();
    
    /**Destructor. Stops the thread, sets the listener to nullptr and sets beat and playStopState to 0*/
    ~Counter();
    
    /** Returns the current beat of the counter. */
    int getBeat();
    
    /** Sets the counter to play or stop. This does not stop the thread from running, but instead tells the listener to stop calling the beatChanged() function.
     @param playState. Sets whether the Listener callback is called or not. A value of 1 will cause the Listener callback to be called and thus start the sequencer, a value of 0 will stop the callback from happening and the sequencer will finish */
    void setPlayStopState(int playState);
    
    /** Sets the interval of the counter. This function is passed the tempo and using that, works out and sets the interval between Listener callbacks. @param tempo this value is the bpm of the sequencer*/
    void setInterval(float tempo);
    
    /** Virtual member function of Thread. This funtion is overridden and is where the majority of the processing is done. The value of beat is updated and the Listener callback is called, assuming playState=1.*/
    void run() override;
    
    /** Counter Listener. This class is to be inheritted by any other classes that need to recieve which beat the sequencer is on. In this case only MainComponent does. */
    class Listener
    {
    public:
        /** Virtual Destructor. Called when a Counter is destroyed. */
        virtual~Listener() {}
        
        /** Called when the beat changes. Broadcasts the current beat to any classes that have inheritted the Counter::Listener.
         @param currentBeat. The beat that the sequencer is currently on. This increments from 0-7, before being brought back to 0 again. 
         @see MainComponent*/
        virtual void beatChanged (const unsigned int currentBeat)=0;
        
    };
    
    /** Registers a listener that will be called when the beat changes. */
    void setListener(Listener* newListener);
    /** Deregisters a previously-registered listener. */
    void removeListener(Listener* listener);
    
private:
    int beat;
    int playStopState;
    float interval;
    Listener* listener;
    
};
#endif /* defined(__JuceBasicAudio__Counter__) */
