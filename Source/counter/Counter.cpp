//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/22/15.
//
//

#include "Counter.h"

Counter::Counter() : Thread ("CounterThread")
{
    startThread();
    listener=nullptr;
    beat = 0;
    playStopState=0;
}

Counter::~Counter()
{
    stopThread(500);
    listener=nullptr;
    beat=0;
    playStopState=0;
}

int Counter::getBeat()
{
    return beat;
}

void Counter::setPlayStopState(int playState)
{
    playStopState = playState;
}

void Counter::setListener(Listener* newListener)
{
    listener=newListener;
}

void Counter::removeListener(Listener* newListener)
{
    listener=nullptr;
}

void Counter::setInterval(float tempo)
{
    interval=60000/tempo;
}

void Counter::run()
{
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        if (playStopState==1)
        {
            beat++;
            if (listener!=nullptr) {
                listener->beatChanged(beat);
            }
            Time::waitForMillisecondCounter (time + interval);
            if (beat==8) {
                beat=0;
            }
        }
        else
            beat=0;
    }
    
}

