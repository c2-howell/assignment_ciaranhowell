/*
 ==============================================================================
 
 Audio.h
 Created: 13 Nov 2014 8:14:40am
 Author:  Tom Mitchell
 
 ==============================================================================
 */

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Audio Class. Contains all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "SinOscillator.h"
#include "SquareOscillator.h"
#include "TriangleOscillator.h"
#include "SawtoothOscillator.h"



class Audio :  // public MidiInputCallback,
public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
   // void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    /** Audio Device Callback. All samples are sent to the output in here. */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    /** Sets all the oscillator's sample rate's to that of the Audio IO Device
     @ param device. A pointer to the current Audio IO Deice. */
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /** Audio IO Device Destructor. */
    void audioDeviceStopped() override;
    
    /** Sets the value of gain within Audio. This function is called within MainComponent when the gainSlider is changed. The value is then passed through to oscillator.setGain()
     @ param gain. The value of the gainSlider. */
    void setGain(float gain);
    
    /** Sets the Oscillator type Id. This function is called within MainComponent when the oscSelector ComboBox is changed.
     @param iD. The Id value of the oscillator type that is currently selected. */
    void getID(int iD);
    
    
    /** Sends the selected Oscillator type, gain and the noteofScale to audio. This function is called within the beatChanged function that MainComponent has inheritted from Counter::Listener.
     @ param osc. Current Oscillator type.
     @ param gain. Current value of gain.
     @ param noteofScale. The current note of scale selected.*/
    void generateNote(float osc, float gain, int noteofScale);
    
    /** Sets the current key. Sets the root note of the scale. This function is called within MainComponent when the keySelector ComboBox is changed.
     @ param key. The Midi number of the root note of the selected scale. */
    void setKey(int key);
    
    /** Sets the current scale to be used. This function is called within MainComponent when the scaleSelector ComboBox is changed.
     @ param scale. The Id of the selected type of scale. The scales are arrays that are initiated in Audio, and the selected Id decides which array is used. */
    void setScale(int scale);
    
    
    
    
private:
    AudioDeviceManager audioDeviceManager;
    SinOscillator sinOsc;
    SquareOscillator squareOsc;
    TriangleOscillator triOsc;
    SawtoothOscillator sawOsc;
    float sampleRate;
    int oscId;
    int keyRoot;
    int scaleId;
    int majorScale[8];
    int naturalMinorScale[8];
    int harmonicMinorScale[8];
    int currentScale[8];
    /*float gainReceived;
     float frequency;
     float phasePos;
     float sampleRate;
     float currentSample;*/
    
};



#endif  // AUDIO_H_INCLUDED
