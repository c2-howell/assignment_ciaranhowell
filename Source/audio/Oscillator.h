//
//  Oscillator.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#ifndef __JuceBasicAudio__Oscillator__
#define __JuceBasicAudio__Oscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"

/** Oscillator Class. Base class for all oscillator types to derive from. */

class Oscillator
{
public:
    /** Constructor. */
    Oscillator();
    
    /** Destructor. */
    virtual ~Oscillator();
    
    
    
    /** Sets the frequency of the oscillator. The frequency is worked out by a combination of the keySelector and scaleSelector(MainComponent) and the noteSelector(ButtonRow). The corresponding Midi note number is then converted into Hz and sent to this function via the Audio::generateNote() function.
     @ param freq. The frequency needed for the current beat. */
    void setFreq(float freq);
    
    /** Sets the amplitude of the oscillator. This function is called within Audio (getGain()) which, in turn was called in MainComponent when the gainSlider is changed.
     @ param amp. The max amplitude of the oscillator. */
    void setAmp(float amp);
    
    /** Sets the sample rate of the oscillator to the current sample rate of the Audio IO Device.
     @param sr. The current sample rate. */
    void setSR(float sr);
    
    /** Trigger the note. Sets the envpeakGain to the value of gain
     @ param gain. The current value of gain*/
    void triggerPulse (float gain);
    
    /** Virtual Member function to be overwritten by specific oscillator types.
     @ param currentPhase. The current phase position of the oscillator. */
    virtual float renderWaveShape(const float currentPhase) = 0;
    
    /** Returns the current sample value. It does this by generating the envelope then mutliplying the current envelope gain by the sample produced within the renderWaveShape function.
     @ return current sample. */
    float getSample();
    
    
    
    
private:
    float gain, targetGain;
    float frequency;
    float phasePos, phaseInc;
    float sampleRate;
    float currentSample;
    float difference, sampleAmount;
    
    float envGain;
    float envPeekGain;
    
};


#endif /* defined(__JuceBasicAudio__Oscillator__) */
