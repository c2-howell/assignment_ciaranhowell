//
//  SqaureOscillator.cpp
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#include "SquareOscillator.h"

float SquareOscillator::renderWaveShape(const float currentPhase)
{
    if (currentPhase<M_PI)
        return 1;
    else
        return 0;
    
}