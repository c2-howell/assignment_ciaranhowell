//
//  TriangleOscillator.cpp
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#include "TriangleOscillator.h"

float TriangleOscillator::renderWaveShape(const float currentPhase)
{
    if (currentPhase<M_PI)
        return -1+((2/M_PI)*currentPhase);
    else
        return 3-((2/M_PI)*currentPhase);
}