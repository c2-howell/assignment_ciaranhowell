//
//  SqaureOscillator.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#ifndef __JuceBasicAudio__SqaureOscillator__
#define __JuceBasicAudio__SqaureOscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

/**SquareOscillator Class. Derived Class of Oscillator, that generates a square wave*/
class SquareOscillator : public Oscillator
{
public:
    /** Returns current sample value for SquareOscillator.
     @param currentPhase. The current phase position of the oscillator.
     @return currentSample. The current sample of SquareOscillator. */
    float renderWaveShape(const float currentPhase) override;
    
};
#endif /* defined(__JuceBasicAudio__SqaureOscillator__) */
