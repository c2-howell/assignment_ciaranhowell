//
//  TriangleOscillator.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#ifndef __JuceBasicAudio__TriangleOscillator__
#define __JuceBasicAudio__TriangleOscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

/**TriangleOscillator Class. Derived Class of Oscillator, that generates a triangle wave*/
class TriangleOscillator : public Oscillator
{
public:
    /** Returns current sample value for TriangleOscillator.
     @param currentPhase. The current phase position of the oscillator.
     @return currentSample. The current sample of TriangleOscillator. */
    float renderWaveShape(const float currentPhase) override;
    
};
#endif /* defined(__JuceBasicAudio__TriangleOscillator__) */
