/*
 ==============================================================================
 
 Audio.cpp
 Created: 13 Nov 2014 8:14:40am
 Author:  Tom Mitchell
 
 ==============================================================================
 */

#include "Audio.h"


Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
   // audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    
//    audioDeviceManager.addMidiInputCallback (String::empty, this);
//    audioDeviceManager.addAudioCallback (this);
    
    oscId=1;
    
    majorScale[0]=0;
    majorScale[1]=2;
    majorScale[2]=4;
    majorScale[3]=5;
    majorScale[4]=7;
    majorScale[5]=9;
    majorScale[6]=11;
    majorScale[7]=12;
    
    naturalMinorScale[0]=0;
    naturalMinorScale[1]=2;
    naturalMinorScale[2]=3;
    naturalMinorScale[3]=5;
    naturalMinorScale[4]=7;
    naturalMinorScale[5]=8;
    naturalMinorScale[6]=10;
    naturalMinorScale[7]=12;
    
    harmonicMinorScale[0]=0;
    harmonicMinorScale[1]=2;
    harmonicMinorScale[2]=3;
    harmonicMinorScale[3]=5;
    harmonicMinorScale[4]=7;
    harmonicMinorScale[5]=8;
    harmonicMinorScale[6]=11;
    harmonicMinorScale[7]=12;
    
    
    
    
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
   // audioDeviceManager.removeMidiInputCallback (String::empty, this);
}



void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                   int numInputChannels,
                                   float** outputChannelData,
                                   int numOutputChannels,
                                   int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
   // sinOsc.setFreq (100);
    
    while(numSamples--)
    {
       float temp;         
        if(oscId==1){
            temp = sinOsc.getSample();
        }
        
        if(oscId==2){
            temp = squareOsc.getSample();
        }
        
        if(oscId==3){
            temp = sawOsc.getSample();
        }
        
        if(oscId==4){
            temp = triOsc.getSample();
        }
        
        *outL = temp;
        *outR = temp;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    sampleRate=device->getCurrentSampleRate();
    sinOsc.setSR(sampleRate);
    squareOsc.setSR(sampleRate);
    triOsc.setSR(sampleRate);
    sawOsc.setSR(sampleRate);
    
    
}

void Audio::audioDeviceStopped()
{
    
}

void Audio::setGain(float gain)
{
    sinOsc.setAmp(gain);
    squareOsc.setAmp(gain);
    triOsc.setAmp(gain);
    sawOsc.setAmp(gain);
    
}

void Audio::getID(int iD)
{
    oscId=iD;
}


void Audio::setKey(int key)
{
    keyRoot=key;
}

void Audio::setScale(int scale)
{
    scaleId=scale;
}


void Audio::generateNote(float osc, float gain, int noteOfScale)
{
    for (int i=0; i<8; i++) {
        if (scaleId==1)
            currentScale[i]=majorScale[i];
        else if (scaleId==2)
            currentScale[i]=naturalMinorScale[i];
        else if (scaleId==3)
            currentScale[i]=harmonicMinorScale[i];
    }
    
    std::cout<<"Generate Note Reached:"<<"\n";
    
    if (osc==1)
    {
        
        std::cout<<"SinOscillator Reached:"<<"\n"<<gain<<"\n"<<noteOfScale<<"\n"<<keyRoot<<"\n"<<osc<<"\n";
        
//        sinOsc.setAmp(gain);
        sinOsc.setFreq(MidiMessage::getMidiNoteInHertz(currentScale[noteOfScale] + keyRoot));
        sinOsc.triggerPulse(1.0);
    }
    else if (osc==2) {
//        squareOsc.setAmp(gain);
        squareOsc.setFreq(MidiMessage::getMidiNoteInHertz(currentScale[noteOfScale] + keyRoot));
        squareOsc.triggerPulse(1.0);
    }
    else if (osc==3) {
//        sawOsc.setAmp(gain);
        sawOsc.setFreq(MidiMessage::getMidiNoteInHertz(currentScale[noteOfScale] + keyRoot));
        sawOsc.triggerPulse(1.0);
    }
    else if (osc==4) {
//        triOsc.setAmp(gain);
        triOsc.setFreq(MidiMessage::getMidiNoteInHertz(currentScale[noteOfScale] + keyRoot));
        triOsc.triggerPulse(1.0);
    }
}




