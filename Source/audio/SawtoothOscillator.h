//
//  SawtoothOscillator.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 05/01/2015.
//
//

#ifndef __JuceBasicAudio__SawtoothOscillator__
#define __JuceBasicAudio__SawtoothOscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

/**Sawtooth Oscillator Class. Derived Class of Oscillator, that generates a sawtooth wave*/
class SawtoothOscillator : public Oscillator
{
public:
    /** Returns current sample value for SawtoothOscillator.
     @param currentPhase. The current phase position of the oscillator.
     @return currentSample. The current sample of SawtoothOscillator. */
    float renderWaveShape(const float currentPhase) override;
    
};

#endif /* defined(__JuceBasicAudio__SawtoothOscillator__) */
