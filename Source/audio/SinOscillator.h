//
//  SinOscillator.h
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#ifndef __JuceBasicAudio__SinOscillator__
#define __JuceBasicAudio__SinOscillator__

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.h"

/** SinOscillator Class. Derived Class of Oscillator, that generates a sin wave*/
class SinOscillator : public Oscillator
{
public:
    /** Returns current sample value for SinOscillator.
     @param currentPhase. The current phase position of the oscillator.
     @return currentSample. The current sample of SinOscillator. */
    float renderWaveShape(const float currentPhase) override;
    
};
#endif /* defined(__JuceBasicAudio__SinOscillator__) */
