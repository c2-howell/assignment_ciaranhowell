//
//  SawtoothOscillator.cpp
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 05/01/2015.
//
//

#include "SawtoothOscillator.h"

float SawtoothOscillator::renderWaveShape(const float currentPhase)
{
    return 1-((1/M_PI)*currentPhase);;
}