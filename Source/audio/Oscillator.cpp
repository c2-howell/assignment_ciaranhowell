//
//  Oscillator.cpp
//  JuceBasicAudio
//
//  Created by Ciaran Howell on 1/5/15.
//
//

#include "Oscillator.h"

Oscillator::Oscillator()
{
    gain=0.f;
    targetGain=0.f;
    frequency=440.f;
    phasePos=0.f;
    envGain = 0;
    envPeekGain = 0;
}

Oscillator::~Oscillator()
{
    
}

void Oscillator::setSR(float sr)
{
    sampleRate=sr;
}

void Oscillator::setFreq(float freq)
{
    frequency = freq;
    phaseInc = (2 * M_PI *  frequency ) / sampleRate ;
    
}

void Oscillator::setAmp(float amp)
{
    gain = amp;
}



float Oscillator::getSample()
{
       
    if (envPeekGain != envGain )
    {
        if (envGain < envPeekGain)
        {
            envGain += 0.001;
        }
        else if (envGain > envPeekGain)
        {
            envPeekGain = 0.0;
            envGain -= 0.0001;
            if (envGain < 0)
            {
                envGain = 0.0;
            }
        }
    }
    
    float out = renderWaveShape (phasePos) * gain * envGain;
	phasePos = phasePos + phaseInc;
	if(phasePos  > (2.f * M_PI))
		phasePos -= (2.f * M_PI);
    
    
    
	return out;
    
    
}

void Oscillator::triggerPulse (float gain)
{
    envPeekGain = gain;
}

